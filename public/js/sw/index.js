/**
self.addEventListener('fetch', function(event) {
  // take response object or promise that resolves with the response
  event.respondWith(
    new Response('<p class="a-winner-is-me">Hello winner!</p>', {
      headers: {'Content-Type': 'text/html'}
    })
  );
});
**/
var staticCacheName = 'wittr-static-v4';

self.addEventListener('install', function(event) {
  // TODO: cache /skeleton rather than the root page
  var urlsToCache = [
    '/skeleton',
    'js/main.js',
    'css/main.css',
    'imgs/icon.png',
    'https://fonts.gstatic.com/s/roboto/v15/2UX7WLTfW3W8TclTUvlFyQ.woff',
    'https://fonts.gstatic.com/s/roboto/v15/d-6IYplOFocCacKzxwXSOD8E0i7KZn-EPnyo3HZu7kw.woff'
  ];

  event.waitUntil(
    // TODO: change the site's theme, eg swap the vars in public/scss/_theme.scss
    // Ensure at least $primary-color changes
    // TODO: change cache name to 'wittr-static-v2'
    caches.open(staticCacheName).then(function(cache) {
      return cache.addAll(urlsToCache);
    })
  );
});

self.addEventListener('activate', function(event) {
  event.waitUntil(
    // TODO: remove the old cache
    caches.keys().then(function(cacheNames) {
      return Promise.all(
        cacheNames.filter(function(cacheName) {
          return cacheName.startsWith('wittr-') &&
                 cacheName != staticCacheName;
        }).map(function(cacheName) {
          return caches.delete(cacheName);
        })
      )
    })
  );
});

self.addEventListener('fetch', function(event) {
  // TODO: respond to requests for the root page with
  // the page skeleton from the cache (serve-skeleton)
  var requestUrl = new URL(event.request.url);

  if (requestUrl.origin == location.origin){
    if (requestUrl.pathname === '/') {
      event.respondWith(caches.match('/skeleton'));
      return;
    }
  }

  event.respondWith(
    caches.match(event.request).then(function(response) {
      return response || fetch(event.request);
    })
  );
});

// TODO: listen for the "message" event, and call
// skipWaiting if you get the appropriate message
self.addEventListener('message', function(event) {
  if (event.data.action == 'skipWaiting') {
    console.log('equal to skip waiting');
    self.skipWaiting();
  }
});

/** fetch lets you make network requests and lets you read the response
    console.log(event.request);
    if (event.request.url.endsWith('.jpg')) {
      event.respondWith(
        fetch('/imgs/dr-evil.gif')
      )
    }
    TODO: response as a gif
self.addEventListener('fetch', function(event) {
  console.log(event.request);
    event.respondWith(
      fetch(event.request).then(function(response) {
        if (response.status === 404 ) {
          console.log(response);
          return fetch('/imgs/dr-evil.gif');
        }
        return response;
      }).catch(function() {
        return new Response("Uh oh, that totally failed!");
      })
    )
})
**/
